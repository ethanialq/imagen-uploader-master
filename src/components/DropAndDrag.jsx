import React, { useState } from 'react'
import st from './DropAndDrag.module.css'
import img from '../assets/image.svg'

const DropAndDrag = () => {
    const [file, setFile] = useState(null)

    const handleChangeImg = e => {
        console.log(e.target.files)
    }
    
    return (
        <div className={st.card}>
            <h2>Upload your image</h2>
            <span>File should be Jpeg, Jpg, png ...</span>
            <form>
                <div className={st.drop}>
                    <img src={img} alt="imagen" />
                    <span>Drag and drop your image here</span>
                    <input type="file" name="img" id="img" accept='image/*' onChange={handleChangeImg}/>
                </div>
                <span>or</span>
                <div className={st.btn}>
                    <span>chose file</span>
                    <input type="file" name="imgBtn" id="imgBtn" accept='image/*' onChange={handleChangeImg} />
                </div>
            </form>
        </div>
    )
}

export default DropAndDrag