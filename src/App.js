import React from "react";
import DropAndDrag from "./components/DropAndDrag";

function App() {
    
    return (
        <div className="app">
            <DropAndDrag />
        </div>
    );
}

export default App;
